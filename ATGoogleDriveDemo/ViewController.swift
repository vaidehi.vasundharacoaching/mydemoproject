//
//  ViewController.swift
//  ATGoogleDriveDemo
//
//  Created by Dejan on 09/04/2018.
//  Copyright © 2018 Dejan. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST

class ViewController: UIViewController {

    @IBOutlet weak var resultsLabel: UILabel!
    
    fileprivate let service = GTLRDriveService()
    private var drive: ATGoogleDrive?
    @IBOutlet var imgDisplay: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGoogleSignIn()
        
        drive = ATGoogleDrive(service)
        
        view.addSubview(GIDSignInButton())
        
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        // create a name for your image
        let fileURL = documentsDirectoryURL.appendingPathComponent("logo.png")
        
        
        if !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try UIImagePNGRepresentation(imgDisplay.image!)!.write(to: fileURL)
                print("Image Added Successfully")
            } catch {
                print(error)
            }
        } else {
            print("Image Not Added")
        }
    }
    
    private func setupGoogleSignIn() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = [kGTLRAuthScopeDriveFile]
        GIDSignIn.sharedInstance().signInSilently()
    }
    
    // MARK: - Actions
    
    
    
    
    
    @IBAction func uploadAction(_ sender: Any) {
        if let documentsDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
            let testFilePath = documentsDir.appendingPathComponent("logo.png").path
            drive?.uploadFile("agostini_tech_demo", filePath: testFilePath, MIMEType: "image/png") { (fileID, error) in
                print("Upload file ID: \(fileID!); Error: \(error?.localizedDescription)")
            }
        }
    }
    @IBAction func listAction(_ sender: Any) {
        drive?.listFilesInFolder("agostini_tech_demo") { (files, error) in
            guard let fileList = files else {
                print("Error listing files: \(error?.localizedDescription)")
                return
            }
            
            self.resultsLabel.text = fileList.files?.description
        }
    }
    
    @IBAction func btnDownload(_ sender: Any)
    {
        drive?.download("1XM6fqb_0i-ZhXRgf-V_vHJbMqm_RW-dW", onCompleted: { (datas, Error) in
            print("Download : \(datas!)")
        })
    }
    
}

// MARK: - GIDSignInDelegate
extension ViewController: GIDSignInDelegate
{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if let _ = error {
            service.authorizer = nil
        } else {
            service.authorizer = user.authentication.fetcherAuthorizer()
        }
    }
}

// MARK: - GIDSignInUIDelegate
extension ViewController: GIDSignInUIDelegate {}
